
#ifndef SEMESTRALKA_EXT_H
#define SEMESTRALKA_EXT_H
#define ID_ITEM_FREE 0
#define I_NODE_AMOUNT 10
#define CLUSTER_SIZE 16384
#define BITMAP_MODE_REMOVE 0
#define BITMAP_MODE_ADD 1
// priklad - verze 2020-01
// jedná se o SIMULACI souborového systému


#include <stdbool.h>
#include <stdlib.h>
#include "string.h"
/* Used for adding information to bitmap */
static const u_char binary[8] = {
    0b10000000,
    0b01000000,
    0b00100000,
    0b00010000,
    0b00001000,
    0b00000100,
    0b00000010,
    0b00000001,
};
/* Inversed binary to remove information from bitmap */
/* Inversed array positions due to easier access */
static const u_char binary_inverse[8] = {
    0b01111111,
    0b10111111,
    0b11011111,
    0b11101111,
    0b11110111,
    0b11111011,
    0b11111101,
    0b11111110,
};


typedef struct{
    char signature[10];              //login autora FS
    char volume_descriptor[20];    //popis vygenerovaného FS
    int32_t disk_size;              //celkova velikost VFS
    int32_t cluster_size;           //velikost clusteru
    int32_t cluster_count;          //pocet clusteru
    int32_t bitmapi_start_address;  //adresa pocatku bitmapy i-uzlů
    int32_t bitmap_start_address;   //adresa pocatku bitmapy datových bloků
    int32_t inode_start_address;    //adresa pocatku  i-uzlů
    int32_t data_start_address;     //adresa pocatku datovych bloku
}superblock;

typedef struct  {
    int32_t nodeid;                 //ID i-uzlu, pokud ID = ID_ITEM_FREE, je polozka volna
    bool isDirectory;               //soubor, nebo adresar
    int8_t references;              //počet odkazů na i-uzel, používá se pro hardlinky
    int32_t file_size;              //velikost souboru v bytech
    int32_t direct[5];              // 1. přímý odkaz na datové bloky
    int32_t indirect1;              // 1. nepřímý odkaz (odkaz - datové bloky)
    int32_t indirect2;              // 2. nepřímý odkaz (odkaz - odkaz - datové bloky)
}pseudo_inode;


typedef struct{
    int32_t inode;                  
    char item_name[12];             
}directory_item;


// funkce pro predvyplneni struktury sb
void fill_default_sb(superblock *sb, int32_t cluster_count);
void create_root_dir(superblock *sb, FILE *file);
void print_i_node_bitmap(u_char *inode_bitmap);
void print_data_bitmap(superblock *sb, u_char *data_bitmap);
int find_empty_i_node_in_bitmap(u_char *inode_bitmap);
int find_empty_data_node_in_bitmap(superblock *sb, u_char *data_bitmap);
int set_i_node_bitmap_bit(u_char **inode_bitmap, int position, int mode);
int set_data_bitmap_bit(u_char **data_bitmap, int position, int mode);
void init_bitmap(u_char **bitmap, int array_element_count);
pseudo_inode *init_pseudo_inode(int32_t nodeid, int32_t file_size, bool is_directory);
void init_directory_info(superblock *sb, FILE *file);
char** split_message(char *message, int *message_len);
pseudo_inode *get_inode_by_id(superblock *sb, char **argv, int current_inode_pointer);
directory_item *init_directory_item(int32_t inode, char *name);
int is_free_ref_space(int array_size, directory_item *directories);
int get_mkdir_offset(superblock *sb, pseudo_inode *inode);
directory_item *load_directories_from_file(superblock *sb, int inode_id, u_char *inode_bitmap, u_char *data_bitmap, char **argv);
void create_new_dir_content(superblock *sb, pseudo_inode *inode, FILE *file, int32_t parent_inode);
int32_t *load_indirect_cluster_ref(superblock *sb, pseudo_inode *inode, char**argv, int *get_cluster_count);
directory_item *load_directory_chunk(superblock *sb, int32_t data_bitmap_position, int32_t *array_size, char **argv, pseudo_inode *inode);
int32_t get_directory_count(superblock *sb, char **argv, pseudo_inode *inode, int32_t data_bit);
int32_t *get_all_refs(superblock *sb, pseudo_inode *inode, char**argv, int *get_cluster_count);
void print_inode_info(pseudo_inode *inode);
void print_all_refs(int32_t *refs, int count, bool show_all);
void change_path(char **path, char *cd_result);
int is_amount_of_spaces_free_in_bitmap(superblock *sb, u_char *data_bitmap, int amount);
void write_new_directory_item_to_memory(superblock *sb, pseudo_inode *current_dir, char **argv, u_char **data_bitmap, FILE *file, char *args, int32_t inode_id);
int get_inode_id_by_name(superblock *sb, char **argv, char *args, pseudo_inode *current_dir);
void remove_directory_item_record_from_inode_data(superblock *sb, char **argv, pseudo_inode *inode, char *args);
int check_is_name_exists(superblock *sb, char **argv, pseudo_inode *inode, char *args);
char **detokenize_path(char *path, int *token_count, bool is_absolute_path);
int check_path_viable(superblock *sb, char **argv, char **detokenized_path, int token_count, int current_inode_pointer);
void change_directory(superblock *sb, int *current_i_node_pointer, char *args, char **argv, char **path);
char *remove_last_path_part(char *path);
char **test_path_detokenization(superblock *sb, char **argv, int current_inode_pointer, char *args, int *path_count);
#endif
