#ifndef __COMMANDS_H__
#define __COMMANDS_H__
#include "ext.h"
void execute_format(superblock **sb, char **argv, u_char **inode_bitmap, u_char **data_bitmap, char *args);
void execute_cd(superblock *sb, int *current_i_node_pointer, char *args, char **argv, char **path);
void execute_info(superblock *sb, char **argv, int current_i_node_pointer, char *args);
void execute_mkdir(superblock *sb, char **argv, int current_i_node_pointer, char *args, u_char **inode_bitmap, u_char **data_bitmap);
void execute_ls(superblock *sb, char **argv, int current_i_node_pointer, u_char *inode_bitmap, u_char *data_bitmap, char *path);
void execute_rmdir(superblock *sb, char **argv, int current_i_node_pointer, char *args, u_char **inode_bitmap, u_char **data_bitmap);
void execute_rm(superblock *sb, char **argv, char *args, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap);
void execute_incp(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap);
void execute_outcp(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer);
void execute_cat(superblock *sb, char **argv, char *args, int current_i_node_pointer, u_char *inode_bitmap, u_char *data_bitmap);
void execute_mv(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap);
// void execute_pwd(superblock *sb, char **argv, int current_i_node_pointer, u_char *inode_bitmap, u_char *data_bitmap);
void execute_pwd(char *path);
void execute_load(superblock **sb, char **argv, char *args, int *current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap, char **path);
void execute_cp(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap);
void execute_ln(superblock *sb, int current_i_node_pointer, char **argv, char *arg1, char *arg2, u_char **inode_bitmap, u_char **data_bitmap);

void test(superblock *sb, char **argv, char *arg1, char *arg2);
void inode_info(superblock *sb, char **argv, char *args);
void indirect_info(superblock *sb, char **argv, char *arg1, char *arg2);
void read_data_block(superblock *sb, char **argv, char *args);

void execute_superblock_info(superblock *sb);
#endif