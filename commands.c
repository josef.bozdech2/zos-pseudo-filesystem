#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "ext.h"
/* ADD CLUSTER COUNT BY PARAM */
void execute_format(superblock **sb, char **argv, u_char **inode_bitmap, u_char **data_bitmap, char *args){
    FILE *file = fopen(argv[1], "wb");
    if(!file){
        exit(2);
    }
    /* CREATE AND WRITE TO FILE SUPERBLOCK */
    int i = 0; 
    for(i = 0; i < strlen(args); i++){
        if(!isdigit(args[i])){
            puts("PARAMETER NOT A NUMBER");
            return;
        }
    }
    int32_t size = atoi(args);
    int32_t cluster_count = size * 1024 * 1024 / CLUSTER_SIZE; //Param [MB]
    fill_default_sb(*sb, cluster_count);
    fwrite(*sb, sizeof(**sb), 1, file);

    init_bitmap(inode_bitmap, I_NODE_AMOUNT);
    *data_bitmap = (u_char *)calloc((*sb)->cluster_count, sizeof(u_char));
    init_bitmap(data_bitmap, (*sb)->cluster_count);

    /* INIT BITMAP BITS FOR ROOT DIRECTORY */
    set_i_node_bitmap_bit(inode_bitmap, 0, BITMAP_MODE_ADD); //CREATE ROOT DIR I NODE IN BITMAP
    set_data_bitmap_bit(data_bitmap, 0, BITMAP_MODE_ADD);

    /* WRITE INODE BITMAP TO FILE */
    fwrite(*inode_bitmap, sizeof(u_char), I_NODE_AMOUNT, file);
    /* WRIOTE DATA BITMAP TO FILE */
    fwrite(*data_bitmap, sizeof(u_char), (*sb)->cluster_count, file);


    // printf("Inode bitmat root folder set!\n");
    /* CREATE ROOT DIRECTORY, FILL INODE BITMAP AND WRITE TO FILE */
    create_root_dir(*sb, file);
    // printf("Filesystem name: %s\nSize of superblock: %lu\n", argv[1], (sizeof(superblock)+sizeof(pseudo_inode)*I_NODE_AMOUNT + I_NODE_AMOUNT + CLUSTER_COUNT));
    fclose(file);
}

void execute_superblock_info(superblock *sb){
    printf("Printing information about superblock!\n");
    printf("signature: %s\nvolume descriptor: %s\ndisk size: %d\ncluster size: %d\ncluster count: %d\n"
               "bitmapi_start_address: %d\nbitmap_start_address: %d\ninode_start_address: %d\ndata_start_address: %d\n",
                sb->signature, sb->volume_descriptor, sb->disk_size, sb->cluster_size, sb->cluster_count, sb->bitmapi_start_address,
                sb->bitmap_start_address, sb->inode_start_address, sb->data_start_address);    
}


void execute_cd(superblock *sb, int *current_i_node_pointer, char *args, char **argv, char **path){
    int path_count = 0;
    int i = 0;
    if(args[0] == '/'){
        *current_i_node_pointer = 0;
    }
    
    char **path_tokens = test_path_detokenization(sb, argv, *current_i_node_pointer, args, &path_count);
    if(!path_tokens){
        puts("PATH NOT FOUND");
        return;
    }

    for(i = 0; i < path_count; i++){
        change_directory(sb, current_i_node_pointer, path_tokens[i], argv, path);
    }
}






/*



                    MKDIR



*/

void execute_mkdir(superblock *sb, char **argv, int current_i_node_pointer, char *args, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *file = fopen(argv[1], "r+b");

    int args_token_count = 0;
    char **detokenized_args = detokenize_path(args, &args_token_count, false);
    int temp_inode_pointer = current_i_node_pointer;
    if(args[0] == '/'){
        temp_inode_pointer = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(args);
    if(!check_path_viable(sb, argv, detokenized_args, args_token_count - 1, temp_inode_pointer)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer);



    /* Nalezení volných pozic pro vložení do bitmap */
    int data_bit = 0;
    int inode_bit = 0;
    /* Vypsání bitmap pro kontrolu */
    // printf("Executing mkdir!\n");
    // printf("Dir name: %s\n", detokenized_args[args_token_count - 1]);
    
    /* Kontrola validnosti jména adresáře */
    if(args != NULL){
        if(strlen(detokenized_args[args_token_count - 1]) < 12){
            /* Pokud je vše správně, vytvoř inode */
            /* GET CURRENT INODE */
            if(!check_is_name_exists(sb, argv, current_dir, detokenized_args[args_token_count - 1])){


                if((find_empty_i_node_in_bitmap(*inode_bitmap) != -1) && (find_empty_data_node_in_bitmap(sb, *data_bitmap) != -1)){
                    /* FIND EMPTY NODES */
                    inode_bit = set_i_node_bitmap_bit(inode_bitmap, find_empty_i_node_in_bitmap(*inode_bitmap), BITMAP_MODE_ADD);
                    data_bit = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
                    /* CREATE NEW I NODE */
                    pseudo_inode *new_dir = init_pseudo_inode(inode_bit, 32, true);
                    new_dir->direct[0] = data_bit;
                    create_new_dir_content(sb, new_dir, file, current_dir->nodeid);
                    fseek(file, sb->inode_start_address + inode_bit * sizeof(*new_dir), SEEK_SET);
                    // printf("Destination to write inode: %d\n", sb->inode_start_address + inode_bit * sizeof(*new_dir));
                    fwrite(new_dir, sizeof(*new_dir), 1, file);
                    
                    write_new_directory_item_to_memory(sb, current_dir, argv, data_bitmap, file, detokenized_args[args_token_count - 1], inode_bit);
                    
                    /* Print bitmapy */
                    // printf("Added bit to inodes: %d\nAdded bit to data: %d\n", inode_bit, data_bit);
                    // print_data_bitmap(sb, *data_bitmap);
                    // print_i_node_bitmap(*inode_bitmap);

        
                }
                else
                {
                    puts("BITMAPS FULL");
                }
            }
            else{
                puts("NAME ALREADY EXISTS");
            }

        }
        else
        {
            puts("NAME TOO LONG");
        }
    }
    else
    {
        puts("NO DIRECTORY NAME");
    }
    
    /* REWRITE BITMAPS */
    fseek(file, sb->bitmapi_start_address, SEEK_SET);
    // printf("Writing to bitmaps, pos: %d\n", sb->bitmapi_start_address);
    fwrite(*inode_bitmap, sizeof(u_char), I_NODE_AMOUNT, file);
    fwrite(*data_bitmap, sizeof(u_char), sb->cluster_count, file);
    fclose(file);
}

/* WRITE TO INDIRECT BITS */
void execute_incp(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *filesystem = fopen(argv[1], "r+b");
    FILE *new_file = fopen(arg1, "rb");

    int args_token_count = 0;
    char **detokenized_args = detokenize_path(arg2, &args_token_count, false);
    int temp_inode_pointer = current_i_node_pointer;
    if(arg2[0] == '/'){
        temp_inode_pointer = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(arg2);
    if(!check_path_viable(sb, argv, detokenized_args, args_token_count - 1, temp_inode_pointer)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_inode = get_inode_by_id(sb, argv, temp_inode_pointer);

    if(get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_inode) != -1){
        puts("NAME ALREADY EXISTS");
        return;
    }
    if(strlen(detokenized_args[args_token_count - 1]) >= 12){
        puts("NAME TOO LONG");
        return;
    }


    int i = 0, j = 0;
    long file_size = 0;
    int32_t inode_bit = 0;
    fseek(new_file, 0L, SEEK_END);
    file_size = ftell(new_file);
    int cluster_count = file_size / CLUSTER_SIZE;

    /* READ ALL DATA FROM FILE */
    u_char *message = (char *)malloc(sizeof(char) * file_size + 1);
    fseek(new_file, 0L, SEEK_SET);
    fread(message, sizeof(*message), file_size, new_file);
    // printf("File content:\n", message);
    // for(i = 0; i < file_size; i++){
    //     printf("%c", message[i]);
    // }
    // printf("\n");
    int remaining_data = file_size;

    if(file_size % CLUSTER_SIZE > 0){
        cluster_count++;
    }
    int32_t *data_bits = (int32_t *)calloc(cluster_count, sizeof(int32_t));
    int32_t *indirect_data_bits = NULL;
    if(cluster_count > 5){
        indirect_data_bits = (int32_t *)calloc(cluster_count - 5, sizeof(int32_t));
    }
    if(is_amount_of_spaces_free_in_bitmap(sb, *data_bitmap, cluster_count) && find_empty_i_node_in_bitmap(*inode_bitmap)){
        inode_bit = set_i_node_bitmap_bit(inode_bitmap, find_empty_i_node_in_bitmap(*inode_bitmap), BITMAP_MODE_ADD);
        /* WRITE NEW RECORD TO CURRENT INODE */
        write_new_directory_item_to_memory(sb, current_inode, argv, data_bitmap, filesystem, detokenized_args[args_token_count - 1], inode_bit);
        // printf("I have written new record %d to current inode!\n", inode_bit);
        /* CREATE NEW INODE */
        pseudo_inode *file_inode = init_pseudo_inode(inode_bit, file_size, false);
        /* REQUEST CLUSTER_COUNT AMOUNT OF BITS FOR DATA */
        for(i = 0; i < cluster_count; i++){
            data_bits[i] = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
        }
        if(cluster_count <= 5){
            /*IF CLUSTER COUNT SMALLER THAN 5, SET DIRECTS */
            for (i = 0; i < cluster_count; i++){
                u_char *message_chunk = (char *)calloc(CLUSTER_SIZE + 1, sizeof(u_char));
                file_inode->direct[i] = data_bits[i];
                fseek(filesystem, sb->data_start_address + file_inode->direct[i] * CLUSTER_SIZE, SEEK_SET);
                memcpy(message_chunk, message + (i * CLUSTER_SIZE), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data);
                fwrite(message_chunk, sizeof(u_char), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, filesystem);
                // printf("Message chunk: %s\n", message_chunk);
                remaining_data -= CLUSTER_SIZE;
                free(message_chunk);
            }
        }
        else{
            /* IF NOT, REQUEST INDIRECT BIT */
            file_inode->indirect1 = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
            /* WRITE DIRECT BITS TO INODE */
            for (i = 0; i < 5; i++){
                u_char *message_chunk = (char *)calloc(CLUSTER_SIZE + 1, sizeof(u_char));
                file_inode->direct[i] = data_bits[i];
                fseek(filesystem, sb->data_start_address + file_inode->direct[i] * CLUSTER_SIZE, SEEK_SET);
                memcpy(message_chunk, message + (i * CLUSTER_SIZE), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data);
                fwrite(message_chunk, sizeof(u_char), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, filesystem);
                // printf("Message chunk: %s\n", message_chunk);
                remaining_data -= CLUSTER_SIZE;
                free(message_chunk);
            }
            /* REWRITE INDIRECT BITS */
            for(i = 5; i < cluster_count; i++){
                u_char *message_chunk = (char *)calloc(CLUSTER_SIZE + 1, sizeof(u_char));
                indirect_data_bits[i - 5] = data_bits[i];
                fseek(filesystem, sb->data_start_address + indirect_data_bits[i - 5] * CLUSTER_SIZE, SEEK_SET);
                memcpy(message_chunk, message + (i * CLUSTER_SIZE), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data);
                fwrite(message_chunk, sizeof(u_char), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, filesystem);
                // printf("Message chunk: %s\n", message_chunk);
                remaining_data -= CLUSTER_SIZE;
                free(message_chunk);
            }
            fseek(filesystem, sb->data_start_address + file_inode->indirect1 * CLUSTER_SIZE, SEEK_SET);
            fwrite(indirect_data_bits, sizeof(*indirect_data_bits), cluster_count - 5, filesystem);
        }
        
        /* WRITE NEW INODE */
        fseek(filesystem, sb->inode_start_address + inode_bit * sizeof(*file_inode), SEEK_SET);
        fwrite(file_inode, sizeof(*file_inode), 1, filesystem);

        // printf("I will now add %d clusters to data_bitmap!\nFile size: %d\n", cluster_count, file_size);
        



        /* REWRITE BITMAPS */
        fseek(filesystem, sb->bitmapi_start_address, SEEK_SET);
        fwrite(*inode_bitmap, sizeof(u_char), I_NODE_AMOUNT, filesystem);
        fwrite(*data_bitmap, sizeof(u_char), sb->cluster_count, filesystem);
    }
    fclose(filesystem);
    fclose(new_file);
}


void test(superblock *sb, char **argv, char *arg1, char *arg2){
    FILE *file = fopen(argv[1], "rb");
    int32_t test = 0;
    int i = 0;
    directory_item *directories = load_directory_chunk(sb, atoi(arg1), &test, argv, get_inode_by_id(sb, argv, arg2 == NULL ? 0 : atoi(arg2)));
    for(i = 0; i < CLUSTER_SIZE / 16; i++){
        printf("Inode: %d\nName: %s\n", directories[i].inode, directories[i].item_name);
    }
}

void inode_info(superblock *sb, char **argv, char *args){
    FILE *file = fopen(argv[1], "rb");
    pseudo_inode *temp = get_inode_by_id(sb, argv, atoi(args));
    print_inode_info(temp);
    free(temp);
    fclose(file);
}

void indirect_info(superblock *sb, char **argv, char *arg1, char *arg2){
    FILE *file = fopen(argv[1], "rb");
    int i = 0;
    int32_t *indirect = (int32_t *)calloc(atoi(arg2), sizeof(int32_t));
    fseek(file, sb->data_start_address + atoi(arg1) * CLUSTER_SIZE, SEEK_SET);
    fread(indirect, sizeof(*indirect), atoi(arg2), file);
    for(; i < atoi(arg2); i++){
        printf("%d ", indirect[i]);
    }
    printf("\n");
}

/* COMPLETE */
void execute_pwd(char *path){
    puts("OK");
    printf("%s\n", path);
}

void execute_rmdir(superblock *sb, char **argv, int current_i_node_pointer, char *args, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *file = fopen(argv[1], "r+b");

    int args_token_count = 0;
    char **detokenized_args = detokenize_path(args, &args_token_count, false);
    int temp_inode_pointer = current_i_node_pointer;
    if(args[0] == '/'){
        temp_inode_pointer = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(args);
    if(!check_path_viable(sb, argv, detokenized_args, args_token_count - 1, temp_inode_pointer)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer);
    if(get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir) == -1){
        puts("DIRECTORY NOT FOUND");
        return;
        
    }
    pseudo_inode *target_dir = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir));
    if(!target_dir->isDirectory){
        puts("NOT A DIRECTORY");
        return;
    }



    int cluster_count = 0;
    int array_size = 0;
    int i = 0, j = 0;
    if(!target_dir->isDirectory){
        puts("NOT A DIRECTORY!");
        return;
    }
    int32_t *all_refs = get_all_refs(sb, target_dir, argv, &cluster_count);
    directory_item *directories = NULL;
    print_all_refs(all_refs, cluster_count, true);
    for(i = 0; i < cluster_count; i++){
        if(all_refs[i] != -1){
            directories = load_directory_chunk(sb, all_refs[i], &array_size, argv, target_dir);
            for(j = 0; j < array_size; j++){
                if(directories[j].inode != -1){
                    printf("Inode id: %d\n", directories[j].inode);
                    if(!strcmp(directories[j].item_name, ".") || !strcmp(directories[j].item_name, "..")){

                    }
                    else{
                        puts("DIRECTORY NOT EMPTY");
                        return;
                    }
                }
            }
            
            
        }
    }
    remove_directory_item_record_from_inode_data(sb, argv, current_dir, detokenized_args[args_token_count - 1]);
    if(target_dir->indirect1 != -1){
        set_data_bitmap_bit(data_bitmap, target_dir->indirect1, BITMAP_MODE_REMOVE);
    }
    for(i = 0; i < cluster_count; i++){
        if(all_refs[i] != -1){
            set_data_bitmap_bit(data_bitmap, all_refs[i], BITMAP_MODE_REMOVE);
        }
    }
    set_i_node_bitmap_bit(inode_bitmap, target_dir->nodeid, BITMAP_MODE_REMOVE);
    // print_i_node_bitmap(*inode_bitmap);
    // print_data_bitmap(sb, *data_bitmap);
    fclose(file);
}

void read_data_block(superblock *sb, char **argv, char *args){
    FILE *file = fopen(argv[1], "rb");
    fseek(file, sb->data_start_address + atoi(args) * CLUSTER_SIZE, SEEK_SET);
    u_char *data = (u_char *)calloc(CLUSTER_SIZE, sizeof(u_char));
    int i = 0;
    fread(data, sizeof(*data), CLUSTER_SIZE, file);
    for(i = 0; i < CLUSTER_SIZE; i++){
        printf("%c", data[i]);
    }
    printf("\n");
}



void execute_cat(superblock *sb, char **argv, char *args, int current_i_node_pointer, u_char *inode_bitmap, u_char *data_bitmap){
    FILE *file = fopen(argv[1], "rb");

    int args_token_count = 0;
    char **detokenized_args = detokenize_path(args, &args_token_count, false);
    int temp_inode_pointer = current_i_node_pointer;
    if(args[0] == '/'){
        temp_inode_pointer = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(args);
    if(!check_path_viable(sb, argv, detokenized_args, args_token_count - 1, temp_inode_pointer)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer);
    if(get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    pseudo_inode *data_node = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir));
    if(data_node->isDirectory){
        puts("NOT A FILE");
        return;
    }



    int cluster_count = 0;
    int32_t *all_refs = get_all_refs(sb, data_node, argv, &cluster_count);
    int i = 0, j = 0;
    for(i = 0; i < cluster_count; i++){
        if(all_refs[i] != -1){
            u_char *data = (u_char *)calloc(CLUSTER_SIZE, sizeof(u_char));
            fseek(file, sb->data_start_address + all_refs[i] * CLUSTER_SIZE, SEEK_SET);
            fread(data, sizeof(*data), CLUSTER_SIZE, file);
            for(j = 0; j < CLUSTER_SIZE; j++){
                printf("%c", data[j]);
            }
            free(data);
        }
    }
    printf("\n");
}

void execute_outcp(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer){
    FILE *filesystem = fopen(argv[1], "rb");
    FILE *file = fopen(arg2, "wb");

    int args_token_count = 0;
    char **detokenized_args = detokenize_path(arg1, &args_token_count, false);
    int temp_inode_pointer = current_i_node_pointer;
    if(arg1[0] == '/'){
        temp_inode_pointer = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(arg2);
    if(!check_path_viable(sb, argv, detokenized_args, args_token_count - 1, temp_inode_pointer)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer);
    if(get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    pseudo_inode *data_node = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir));
    if(data_node->isDirectory){
        puts("NOT A FILE");
        return;
    }
    int remaining_data = data_node->file_size;
    int cluster_count = 0;
    int32_t *all_refs = get_all_refs(sb, data_node, argv, &cluster_count);
    u_char *data = (u_char *)calloc(CLUSTER_SIZE, sizeof(u_char));
    int i = 0, j = 0;
    for(i = 0; i < cluster_count; i++){
        if(all_refs[i] != -1){
            fseek(filesystem, sb->data_start_address + all_refs[i] * CLUSTER_SIZE, SEEK_SET);
            fread(data, sizeof(*data), CLUSTER_SIZE, filesystem);
            fwrite(data, sizeof(*data), remaining_data > CLUSTER_SIZE ? CLUSTER_SIZE : remaining_data, file);
            remaining_data -= CLUSTER_SIZE;
        }
    }
    fclose(file);
    fclose(filesystem);
}




void execute_cp(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *file = fopen(argv[1], "r+b");
    int arg1_token_count = 0, arg2_token_count = 0;
    char **detokenized_arg1 = detokenize_path(arg1, &arg1_token_count, false);
    char **detokenized_arg2 = detokenize_path(arg2, &arg2_token_count, false);
    int temp_inode_pointer1 = current_i_node_pointer;
    int temp_inode_pointer2 = current_i_node_pointer;
    if(arg1[0] == '/'){
        temp_inode_pointer1 = 0;
    }
    if(arg2[0] == '/'){
        temp_inode_pointer2 = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *temp_path2 = (char *)malloc(sizeof(char) * 1000);
    char *arg1_last_removed = remove_last_path_part(arg1);
    char *arg2_last_removed = remove_last_path_part(arg2);
    int i = 0;
    if(!check_path_viable(sb, argv, detokenized_arg1, arg1_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    if(!check_path_viable(sb, argv, detokenized_arg2, arg2_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer1, arg1_last_removed, argv, &temp_path1);
    execute_cd(sb, &temp_inode_pointer2, arg2_last_removed, argv, &temp_path2);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer1);
    if(get_inode_id_by_name(sb, argv, detokenized_arg1[arg1_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    pseudo_inode *current_file = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv,detokenized_arg1[arg1_token_count - 1], current_dir));
    if(current_file->isDirectory){
        puts("NOT A FILE");
        return;
    }
    if(get_inode_id_by_name(sb, argv, detokenized_arg2[arg2_token_count - 1], current_dir) != -1){
        puts("NAME ALREADY EXISTS");
        return;
    }
    pseudo_inode *target_dir = get_inode_by_id(sb, argv, temp_inode_pointer2);
    int inode_bit = set_i_node_bitmap_bit(data_bitmap, find_empty_i_node_in_bitmap(*inode_bitmap), BITMAP_MODE_ADD);
    /* CREATE NEW INODE */
    pseudo_inode *target_file = init_pseudo_inode(inode_bit, current_file->file_size, false);
    // printf("Path 1 node id: %d\nPath 2 node id: %d\n", temp_inode_pointer1, temp_inode_pointer2);
    int ref_count = 0;

    int cluster_count = 0;
    int32_t *all_src_refs = get_all_refs(sb, current_file, argv, &cluster_count);
    int remaining_data = target_file->file_size;
    int32_t *data_bits = (int32_t *)calloc(cluster_count, sizeof(int32_t));
    int32_t *indirect_data_bits = NULL;
    if(cluster_count > 5){
        indirect_data_bits = (int32_t *)calloc(cluster_count - 5, sizeof(int32_t));
    }
    if(is_amount_of_spaces_free_in_bitmap(sb, *data_bitmap, cluster_count) && find_empty_i_node_in_bitmap(*inode_bitmap)){
        inode_bit = set_i_node_bitmap_bit(inode_bitmap, find_empty_i_node_in_bitmap(*inode_bitmap), BITMAP_MODE_ADD);
        /* WRITE NEW RECORD TO CURRENT INODE */
        write_new_directory_item_to_memory(sb, target_dir, argv, data_bitmap, file, detokenized_arg2[arg2_token_count - 1], inode_bit);
        // printf("I have written new record %d to current inode!\n", inode_bit);
        /* REQUEST CLUSTER_COUNT AMOUNT OF BITS FOR DATA */
        for(i = 0; i < cluster_count; i++){
            data_bits[i] = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
        }
        if(cluster_count <= 5){
            /*IF CLUSTER COUNT SMALLER THAN 5, SET DIRECTS */
            for (i = 0; i < cluster_count; i++){
                if(all_src_refs[i] != -1){
                    u_char *message_chunk = (char *)calloc(CLUSTER_SIZE + 1, sizeof(u_char));
                    fseek(file, sb->data_start_address + all_src_refs[i] * CLUSTER_SIZE, SEEK_SET);
                    fread(message_chunk, sizeof(*message_chunk), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, file);
                    target_file->direct[i] = data_bits[i];
                    fseek(file, sb->data_start_address + target_file->direct[i] * CLUSTER_SIZE, SEEK_SET);
                    fwrite(message_chunk, sizeof(u_char), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, file);
                    // printf("Message chunk: %s\n", message_chunk);
                    remaining_data -= CLUSTER_SIZE;
                    free(message_chunk);
                }   
            }
        }
        else{
            /* IF NOT, REQUEST INDIRECT BIT */
            target_file->indirect1 = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
            /* WRITE DIRECT BITS TO INODE */

            for (i = 0; i < 5; i++){
                u_char *message_chunk = (char *)calloc(CLUSTER_SIZE + 1, sizeof(u_char));
                fseek(file, sb->data_start_address + all_src_refs[i] * CLUSTER_SIZE, SEEK_SET);
                fread(message_chunk, sizeof(*message_chunk), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, file);
                target_file->direct[i] = data_bits[i];
                fseek(file, sb->data_start_address + target_file->direct[i] * CLUSTER_SIZE, SEEK_SET);
                fwrite(message_chunk, sizeof(u_char), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, file);
                // printf("Message chunk: %s\n", message_chunk);
                remaining_data -= CLUSTER_SIZE;
                free(message_chunk);
            }
            /* REWRITE INDIRECT BITS */
            for(i = 5; i < cluster_count; i++){
                u_char *message_chunk = (char *)calloc(CLUSTER_SIZE + 1, sizeof(u_char));
                fseek(file, sb->data_start_address + all_src_refs[i] * CLUSTER_SIZE, SEEK_SET);
                fread(message_chunk, sizeof(*message_chunk), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, file);
                indirect_data_bits[i - 5] = data_bits[i];
                fseek(file, sb->data_start_address + indirect_data_bits[i - 5] * CLUSTER_SIZE, SEEK_SET);
                fwrite(message_chunk, sizeof(u_char), (remaining_data > CLUSTER_SIZE) ? CLUSTER_SIZE : remaining_data, file);
                // printf("Message chunk: %s\n", message_chunk);
                remaining_data -= CLUSTER_SIZE;
                free(message_chunk);
            }
            fseek(file, sb->data_start_address + target_file->indirect1 * CLUSTER_SIZE, SEEK_SET);
            fwrite(indirect_data_bits, sizeof(*indirect_data_bits), cluster_count - 5, file);
        }
        
        /* WRITE NEW INODE */
        fseek(file, sb->inode_start_address + inode_bit * sizeof(*target_file), SEEK_SET);
        fwrite(target_file, sizeof(*target_file), 1, file);

        // printf("I will now add %d clusters to data_bitmap!\nFile size: %d\n", cluster_count, target_file->file_size);
        



        /* REWRITE BITMAPS */
        fseek(file, sb->bitmapi_start_address, SEEK_SET);
        fwrite(*inode_bitmap, sizeof(u_char), I_NODE_AMOUNT, file);
        fwrite(*data_bitmap, sizeof(u_char), sb->cluster_count, file);
    }

    fclose(file);
    return;
}

void execute_ls(superblock *sb, char **argv, int current_i_node, u_char *inode_bitmap, u_char *data_bitmap, char *path){
    FILE *file = fopen(argv[1], "rb");
    int i = 0, j = 0;
    int cluster_count = 0;
    int32_t dir_count = 0;
    int token_count = 0;
    int temp_current_inode_id = current_i_node;
    if(path[0] == '/'){
        temp_current_inode_id = 0;
    }
    char *temp_path = (char *)malloc(sizeof(char) * 1000);
    strcpy(temp_path, "");
    char **detokenized_path = detokenize_path(path, &token_count, false);
    if(!check_path_viable(sb, argv, detokenized_path, token_count, current_i_node)){
        puts("PATH NOT FOUND");
    }
    execute_cd(sb, &temp_current_inode_id, path, argv, &temp_path);
    // printf("Temp nodeid: %d\n", temp_current_inode_id);
    pseudo_inode *inode = get_inode_by_id(sb, argv, temp_current_inode_id);
    int32_t *all_refs = get_all_refs(sb, get_inode_by_id(sb, argv, temp_current_inode_id), argv, &cluster_count);
    // print_all_refs(all_refs, cluster_count, true);
    // printf("Cluster count: %d\n", cluster_count);
    for(; i < cluster_count; i++){
        if(all_refs[i] == -1){
            break;
        }
        directory_item *directories = load_directory_chunk(sb, all_refs[i], &dir_count, argv, inode);
        for(j = 0; j < dir_count; j++){
            pseudo_inode *temp = get_inode_by_id(sb, argv, directories[j].inode);
            if(directories[j].inode != -1)
            printf("%c %s\n", temp->isDirectory == 1 ? '+' : '-', directories[j].item_name);
        }
    }
    free(inode);
    fclose(file);
}

void execute_rm(superblock *sb, char **argv, char *args, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *file = fopen(argv[1], "r+b");

    int args_token_count = 0;
    char **detokenized_args = detokenize_path(args, &args_token_count, false);
    int temp_inode_pointer = current_i_node_pointer;
    if(args[0] == '/'){
        temp_inode_pointer = 0;
    }
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(args);
    if(!check_path_viable(sb, argv, detokenized_args, args_token_count - 1, temp_inode_pointer)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer);
    if(get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    pseudo_inode *target_dir = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv, detokenized_args[args_token_count - 1], current_dir));
    if(target_dir->isDirectory){
        puts("NOT A FILE");
        return;
    }

    if(target_dir->references != 1){
        target_dir->references--;
        remove_directory_item_record_from_inode_data(sb, argv, current_dir, detokenized_args[args_token_count - 1]);
        fseek(file, sb->inode_start_address + target_dir->nodeid * sizeof(pseudo_inode), SEEK_SET);
        fwrite(target_dir, sizeof(*target_dir), 1, file);
        fclose(file);
        return;
    }


    int cluster_count = 0;
    int array_size = 0;
    int i = 0, j = 0;
    int32_t *all_refs = get_all_refs(sb, target_dir, argv, &cluster_count);
    print_all_refs(all_refs, cluster_count, true);
    
    remove_directory_item_record_from_inode_data(sb, argv, current_dir, detokenized_args[args_token_count - 1]);
    if(target_dir->indirect1 != -1){
        set_data_bitmap_bit(data_bitmap, target_dir->indirect1, BITMAP_MODE_REMOVE);
    }
    for(i = 0; i < cluster_count; i++){
        if(all_refs[i] != -1){
            set_data_bitmap_bit(data_bitmap, all_refs[i], BITMAP_MODE_REMOVE);
        }
    }
    set_i_node_bitmap_bit(inode_bitmap, target_dir->nodeid, BITMAP_MODE_REMOVE);
    // print_i_node_bitmap(*inode_bitmap);
    // print_data_bitmap(sb, *data_bitmap);
    fclose(file);
    
}
/* COMPLETE */

void execute_mv(superblock *sb, char **argv, char *arg1, char *arg2, int current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *file = fopen(argv[1], "r+b");
    int arg1_token_count = 0, arg2_token_count = 0;
    char **detokenized_arg1 = detokenize_path(arg1, &arg1_token_count, false);
    char **detokenized_arg2 = detokenize_path(arg2, &arg2_token_count, false);
    int temp_inode_pointer1 = current_i_node_pointer;
    int temp_inode_pointer2 = current_i_node_pointer;
    if(arg1[0] == '/'){
        temp_inode_pointer1 = 0;
    }
    if(arg2[0] == '/'){
        temp_inode_pointer2 = 0;
    }
    
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *temp_path2 = (char *)malloc(sizeof(char) * 1000);
    char *arg1_last_removed = remove_last_path_part(arg1);
    char *arg2_last_removed = remove_last_path_part(arg2);
    int i = 0;
    if(!check_path_viable(sb, argv, detokenized_arg1, arg1_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    if(!check_path_viable(sb, argv, detokenized_arg2, arg2_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer1, arg1_last_removed, argv, &temp_path1);
    execute_cd(sb, &temp_inode_pointer2, arg2_last_removed, argv, &temp_path2);
    // printf("Temp inode pointer 1: %d\nTemp inode pointer 2: %d\n", temp_inode_pointer1, temp_inode_pointer2);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer1);
    if(get_inode_id_by_name(sb, argv, detokenized_arg1[arg1_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    pseudo_inode *current_file = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv,detokenized_arg1[arg1_token_count - 1], current_dir));
    if(current_file->isDirectory){
        puts("NOT A FILE");
        return;
    }
    if(get_inode_id_by_name(sb, argv, detokenized_arg2[arg2_token_count - 1], current_dir) != -1){
        puts("NAME ALREADY EXISTS");
        return;
    }
    remove_directory_item_record_from_inode_data(sb, argv, current_dir, detokenized_arg1[arg1_token_count - 1]);
    pseudo_inode *target_dir = get_inode_by_id(sb, argv, temp_inode_pointer2);
    printf("Temp inode pointer 2: %d\nTarget dir nodeid: %d\n", temp_inode_pointer2, target_dir->nodeid);
    write_new_directory_item_to_memory(sb, target_dir, argv, data_bitmap, file, detokenized_arg2[arg2_token_count - 1], current_file->nodeid);
    fclose(file);
}



/* COMPLETE */

void execute_ln(superblock *sb, int current_i_node_pointer, char **argv, char *arg1, char *arg2, u_char **inode_bitmap, u_char **data_bitmap){
    FILE *file = fopen(argv[1], "r+b");
    int arg1_token_count = 0, arg2_token_count = 0;
    int temp_inode_pointer1 = current_i_node_pointer;
    int temp_inode_pointer2 = current_i_node_pointer;
    
    char **detokenized_arg1 = detokenize_path(arg1, &arg1_token_count, false);
    char **detokenized_arg2 = detokenize_path(arg2, &arg2_token_count, false);
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *temp_path2 = (char *)malloc(sizeof(char) * 1000);
    char *arg1_last_removed = remove_last_path_part(arg1);
    char *arg2_last_removed = remove_last_path_part(arg2);
    if(arg1[0] == '/'){
        temp_inode_pointer1 = 0;
    }
    if(arg2[0] == '/'){
        temp_inode_pointer2 = 0;
    }
    





    if(!check_path_viable(sb, argv, detokenized_arg1, arg1_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    if(!check_path_viable(sb, argv, detokenized_arg2, arg2_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    execute_cd(sb, &temp_inode_pointer1, arg1_last_removed, argv, &temp_path1);
    execute_cd(sb, &temp_inode_pointer2, arg2_last_removed, argv, &temp_path2);
    pseudo_inode *target_dir = get_inode_by_id(sb, argv, temp_inode_pointer2);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer1);
    if(get_inode_id_by_name(sb, argv, detokenized_arg1[arg1_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    pseudo_inode *current_file = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv,detokenized_arg1[arg1_token_count - 1], current_dir));
    if(current_file->isDirectory){
        puts("SOURCE NOT A FILE");
        return;
    }
    if(get_inode_id_by_name(sb, argv, detokenized_arg2[arg2_token_count - 1], target_dir) != -1){
        puts("NAME ALREADY EXISTS");
        return;
    }
    current_file->references++;
    fseek(file, sb->inode_start_address + current_file->nodeid * sizeof(pseudo_inode), SEEK_SET);
    fwrite(current_file, sizeof(*current_file), 1, file);
    write_new_directory_item_to_memory(sb, target_dir, argv, data_bitmap, file, detokenized_arg2[arg2_token_count -1], current_file->nodeid);
    fclose(file);
}

void execute_info(superblock *sb, char **argv, int current_i_node_pointer, char *args){
    FILE *file = fopen(argv[1], "rb");
    int arg1_token_count = 0, arg2_token_count = 0;
    int temp_inode_pointer1 = current_i_node_pointer;
    
    char **detokenized_args = detokenize_path(args, &arg1_token_count, false);
    char *temp_path1 = (char *)malloc(sizeof(char) * 1000);
    char *args_last_removed = remove_last_path_part(args);
    if(args[0] == '/'){
        temp_inode_pointer1 = 0;
    }
    





    if(!check_path_viable(sb, argv, detokenized_args, arg1_token_count - 1, temp_inode_pointer1)){
        puts("PATH NOT FOUND");
        return;
    }
    
    execute_cd(sb, &temp_inode_pointer1, args_last_removed, argv, &temp_path1);
    pseudo_inode *current_dir = get_inode_by_id(sb, argv, temp_inode_pointer1);
    if(get_inode_id_by_name(sb, argv, detokenized_args[arg1_token_count - 1], current_dir) == -1){
        puts("FILE NOT FOUND");
        return;
        
    }
    puts("OK");
    int cluster_count = 0;
    int info_cluster_count = 0;
    int i = 0, j = 0;
    pseudo_inode *info_file = get_inode_by_id(sb, argv, get_inode_id_by_name(sb, argv, detokenized_args[arg1_token_count - 1], current_dir));
    int32_t *all_refs = get_all_refs(sb, current_dir, argv, &cluster_count);
    int32_t *all_info_refs = get_all_refs(sb, info_file, argv, &info_cluster_count);
    for(i = 0; i < cluster_count; i++){
        if(all_refs[i] != -1){
            int array_size = 0;
            directory_item *directories = load_directory_chunk(sb, all_refs[i], &array_size, argv, current_dir);
            for(j = 0; j < array_size; j++){
                if(directories[j].inode == info_file->nodeid){
                    printf("Name: %s\nFile size: %d\ni-node: %d\n", directories[j].item_name, info_file->file_size, info_file->nodeid);
                    print_all_refs(all_info_refs, info_cluster_count, false);
                    return;
                }
            }
        }
    }
}

void execute_load(superblock **sb, char **argv, char *args, int *current_i_node_pointer, u_char **inode_bitmap, u_char **data_bitmap, char **path){
    FILE *file = fopen(args, "r");
    char command[1000];
    int param_count = 0;
    char **detokenized_message = NULL;
    while(fgets(command, 1000, file) != NULL){
        if(strcmp(command, "\n")){
            
            /* DETOKENIZE COMMAND */
            command[strlen(command) - 1] = '\0';
            detokenized_message = split_message(command, &param_count);
            // printf("Parameter count: %d\n", param_count);
            /* CP */
            if(!strcmp(detokenized_message[0], "cp")){
                execute_cp(*sb, argv, detokenized_message[1], detokenized_message[2], *current_i_node_pointer, inode_bitmap, data_bitmap);
            }
            /* MV */
            if(!strcmp(detokenized_message[0], "mv")){
                execute_mv(*sb, argv, detokenized_message[1], detokenized_message[2], *current_i_node_pointer, inode_bitmap, data_bitmap);
            }
            /* RM */
            if(!strcmp(detokenized_message[0], "rm")){
                execute_rm(*sb, argv, detokenized_message[1], *current_i_node_pointer, inode_bitmap, data_bitmap);
            }
            /* MKDIR */
            if(!strcmp(detokenized_message[0], "mkdir")){
                if(param_count == 2){
                    execute_mkdir(*sb, argv, *current_i_node_pointer, detokenized_message[1], inode_bitmap, data_bitmap);
                }
                else
                {
                    puts("INVALID AMOUNT OF PARAMETERS");
                }
                
            }
            /* RMDIR */
            if(!strcmp(detokenized_message[0], "rmdir")){
                execute_rmdir(*sb, argv, *current_i_node_pointer, detokenized_message[1], inode_bitmap, data_bitmap);
            }
            /* LS */
            if(!strcmp(detokenized_message[0], "ls")){
                if(param_count == 1){
                    execute_ls(*sb, argv, *current_i_node_pointer, NULL, NULL, ".");
                }
                else{
                    execute_ls(*sb, argv, *current_i_node_pointer, NULL, NULL, detokenized_message[1]);
                }
            }
            /* CAT */
            if(!strcmp(detokenized_message[0], "cat")){
                execute_cat(*sb, argv, detokenized_message[1], *current_i_node_pointer, *inode_bitmap, *data_bitmap);
            }
            /* CD */
            if(!strcmp(detokenized_message[0], "cd")){
                execute_cd(*sb, current_i_node_pointer, detokenized_message[1], argv, path);
            }
            /* PWD */
            if(!strcmp(detokenized_message[0], "pwd")){
                execute_pwd(*path);
            }
            /* INFO */
            if(!strcmp(detokenized_message[0], "info")){
                execute_info(*sb, argv, *current_i_node_pointer, detokenized_message[1]);
            }
            /* INCP */
            if(!strcmp(detokenized_message[0], "incp")){
                if(param_count != 3){
                    puts("INVALID AMOUNT OF PARAMETERS");
                }
                else{
                    execute_incp(*sb, argv, detokenized_message[1], detokenized_message[2], *current_i_node_pointer, inode_bitmap, data_bitmap);
                }
            }
            /* OUTCP */
            if(!strcmp(detokenized_message[0], "outcp")){
                execute_outcp(*sb, argv, detokenized_message[1], detokenized_message[2], *current_i_node_pointer);
            }
            /* LOAD */
            if(!strcmp(detokenized_message[0], "load")){
                // printf("Loading %s\n", detokenized_message[1]);
                execute_load(sb, argv, detokenized_message[1], current_i_node_pointer, inode_bitmap, data_bitmap, path);
            }
            /* LN */
            if(!strcmp(detokenized_message[0], "ln")){
                execute_ln(*sb, *current_i_node_pointer, argv, detokenized_message[1], detokenized_message[2], inode_bitmap, data_bitmap);
            }
            /* FORMAT */
            if(!strcmp(detokenized_message[0], "format")){
                execute_format(sb, argv, inode_bitmap, data_bitmap, detokenized_message[1]);
            }

            
            if(!strcmp(detokenized_message[0], "exit")){
                FILE *file = fopen(argv[1], "r+b");
                fseek(file, (*sb)->bitmapi_start_address, SEEK_SET);
                fwrite(inode_bitmap, sizeof(*inode_bitmap), I_NODE_AMOUNT, file);
                fwrite(data_bitmap, sizeof(*data_bitmap), (*sb)->cluster_count, file);
                fclose(file);
            }
        }
    }
    
    fclose(file);
}