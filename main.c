#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include "commands.h"
#include "ext.h"
int main(int argc, char *argv[]){

    char command[100];
    char **detokenized_message = NULL;
    char *path = (char *)malloc(sizeof(char) * 1000);
    strcpy(path, "/");
    superblock *sb = (superblock *)malloc(sizeof(superblock));
    int param_count = 0;
    int32_t current_i_node_pointer = 0;
    // inode bitmapa
    u_char *inode_bitmap = (u_char *)malloc(sizeof(u_char) * 10);
    init_bitmap(&inode_bitmap, I_NODE_AMOUNT);
    // print_i_node_bitmap(inode_bitmap);
    u_char *data_bitmap = NULL;
    // data bitmapa 
    init_bitmap(&data_bitmap, sb->cluster_count);
    /* Check for valid input */
    
    if(argc != 2){
        puts("Usage: ./fs <filename>");
        return 1;
    }
    /* Check if file already exists */
    if(access(argv[1], F_OK) == 0){
        /* Load superblock */
        // printf("File already exists! Reading superblock!\n");
        FILE *read_file = fopen(argv[1], "rb");
        fread(sb, sizeof(*sb), 1, read_file);
        data_bitmap = (u_char *)calloc(sb->cluster_count, sizeof(u_char));
        fseek(read_file, sb->bitmapi_start_address, SEEK_SET);
        // printf("Opening file, bitmap start address: %d\n", sb->bitmapi_start_address);
        fread(inode_bitmap, sizeof(u_char), I_NODE_AMOUNT, read_file);
        fread(data_bitmap, sizeof(u_char), sb->cluster_count, read_file);
        // print_i_node_bitmap(inode_bitmap);
        // print_data_bitmap(sb, data_bitmap);
        // execute_superblock_info(sb);
        fclose(read_file);
        puts("OK");
    }
    /* Command validation */
    do
    {
        printf(">");
        fgets(command, 100, stdin);
        if(strcmp(command, "\n")){
            
            // printf("%s\n", command);

            /* DETOKENIZE COMMAND */
            command[strlen(command) - 1] = '\0';
            detokenized_message = split_message(command, &param_count);
            // printf("Parameter count: %d\n", param_count);
            /* CP */
            if(!strcmp(detokenized_message[0], "cp")){
                execute_cp(sb, argv, detokenized_message[1], detokenized_message[2], current_i_node_pointer, &inode_bitmap, &data_bitmap);
            }
            /* MV */
            if(!strcmp(detokenized_message[0], "mv")){
                execute_mv(sb, argv, detokenized_message[1], detokenized_message[2], current_i_node_pointer, &inode_bitmap, &data_bitmap);
            }
            /* RM */
            if(!strcmp(detokenized_message[0], "rm")){
                execute_rm(sb, argv, detokenized_message[1], current_i_node_pointer, &inode_bitmap, &data_bitmap);
            }
            /* MKDIR */
            if(!strcmp(detokenized_message[0], "mkdir")){
                if(param_count == 2){
                    execute_mkdir(sb, argv, current_i_node_pointer, detokenized_message[1], &inode_bitmap, &data_bitmap);
                    // printf("free space: %d\n", find_free_ref_space(sb, data_bitmap, get_inode_by_id(sb, argv, current_i_node_pointer), argv));
                }
                else
                {
                    puts("INVALID AMOUNT OF PARAMETERS");
                }
                
            }
            /* RMDIR */
            if(!strcmp(detokenized_message[0], "rmdir")){
                execute_rmdir(sb, argv, current_i_node_pointer, detokenized_message[1], &inode_bitmap, &data_bitmap);
            }
            /* LS */
            if(!strcmp(detokenized_message[0], "ls")){
                if(param_count == 1){
                    execute_ls(sb, argv, current_i_node_pointer, NULL, NULL, ".");
                }
                else{
                    execute_ls(sb, argv, current_i_node_pointer, NULL, NULL, detokenized_message[1]);
                }
            }
            /* CAT */
            if(!strcmp(detokenized_message[0], "cat")){
                execute_cat(sb, argv, detokenized_message[1], current_i_node_pointer, inode_bitmap, data_bitmap);
            }
            /* CD */
            if(!strcmp(detokenized_message[0], "cd")){
                execute_cd(sb, &current_i_node_pointer, detokenized_message[1], argv, &path);
            }
            /* PWD */
            if(!strcmp(detokenized_message[0], "pwd")){
                execute_pwd(path);
            }
            /* INFO */
            if(!strcmp(detokenized_message[0], "info")){
                execute_info(sb, argv, current_i_node_pointer, detokenized_message[1]);
            }
            /* INCP */
            if(!strcmp(detokenized_message[0], "incp")){
                if(param_count != 3){
                    puts("INVALID AMOUNT OF PARAMETERS");
                }
                else{
                    execute_incp(sb, argv, detokenized_message[1], detokenized_message[2], current_i_node_pointer, &inode_bitmap, &data_bitmap);
                }
            }
            /* OUTCP */
            if(!strcmp(detokenized_message[0], "outcp")){
                execute_outcp(sb, argv, detokenized_message[1], detokenized_message[2], current_i_node_pointer);
            }
            /* LOAD */
            if(!strcmp(detokenized_message[0], "load")){
                // printf("Loading %s\n", detokenized_message[1]);
                execute_load(&sb, argv, detokenized_message[1], &current_i_node_pointer, &inode_bitmap, &data_bitmap, &path);
            }
            /* LN */
            if(!strcmp(detokenized_message[0], "ln")){
                execute_ln(sb, current_i_node_pointer, argv, detokenized_message[1], detokenized_message[2], &inode_bitmap, &data_bitmap);
            }
            /* FORMAT */
            if(!strcmp(detokenized_message[0], "format")){
                execute_format(&sb, argv, &inode_bitmap, &data_bitmap, detokenized_message[1]);
            }

            // if(!strcmp(detokenized_message[0], "test")){
            //     printf("In test!\n");
            //     test(sb, argv, detokenized_message[1], param_count == 3 ? detokenized_message[2] : NULL);
            // }

            // if(!strcmp(detokenized_message[0], "ii")){
            //     inode_info(sb, argv, detokenized_message[1]);
            // }

            // if(!strcmp(detokenized_message[0], "pii")){
            //     indirect_info(sb, argv, detokenized_message[1], detokenized_message[2]);
            // }

            // if(!strcmp(detokenized_message[0], "data")){
            //     read_data_block(sb, argv, detokenized_message[1]);
            // }

            // if(!strcmp(detokenized_message[0], "path")){
            //     int count = 0;
            //     test_path_detokenization(sb, argv, current_i_node_pointer, detokenized_message[1], &count);
            // }

            // if(!strcmp(detokenized_message[0], "sinfo")){
            //     execute_superblock_info(sb);
            // }

            if(!strcmp(detokenized_message[0], "bitmap")){
                print_data_bitmap(sb, data_bitmap);
                print_i_node_bitmap(inode_bitmap);
            }
            param_count = 0;
            if(!strcmp(detokenized_message[0], "exit")){
                FILE *file = fopen(argv[1], "r+b");
                fseek(file, sb->bitmapi_start_address, SEEK_SET);
                fwrite(inode_bitmap, sizeof(*inode_bitmap), I_NODE_AMOUNT, file);
                fwrite(data_bitmap, sizeof(*data_bitmap), sb->cluster_count, file);
                fclose(file);
            }
        }
    } while (!strcmp(command, "\n") || strcmp(detokenized_message[0], "exit"));
    

    return 0;
}