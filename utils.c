#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ext.h"

void fill_default_sb(superblock *sb, int32_t cluster_count) {
   // add disk_size param
   strcpy(sb->signature, "== ext ==");
   strcpy(sb->volume_descriptor, "popis bla bla bla");
   sb->disk_size = (int32_t)cluster_count * CLUSTER_SIZE;
   sb->cluster_size = CLUSTER_SIZE; // 64B
   sb->cluster_count = cluster_count;
   sb->bitmapi_start_address = sizeof(superblock); // konec sb
   sb->bitmap_start_address = sb->bitmapi_start_address + I_NODE_AMOUNT; // 80 bitů bitmapa
   sb->inode_start_address = sb->bitmap_start_address + cluster_count;
   sb->data_start_address = sb->inode_start_address + 8 * I_NODE_AMOUNT * sizeof(pseudo_inode);
}

void create_root_dir(superblock *sb, FILE *file){
   // printf("Initiating root dir!\n");
   /* WRITE ROOT DIR TO FILE */

   //TODO: Předělat logiku na cluster size !!!! TODO:
   pseudo_inode *root_dir = init_pseudo_inode(0, 32, true);
   root_dir->direct[0] = 0;
   fwrite(root_dir, sizeof(*root_dir), 1, file);

   /* MOVE FSEEK TO DATA START ADDRESS */
   fseek(file, sb->data_start_address, SEEK_SET);
   
   /* WRITE ROOT DIR ÍNFO */
   directory_item *root_dir_item = init_directory_item(0, ".");
   directory_item *root_parent_dir_item = init_directory_item(0, "..");
   fwrite(root_dir_item, sizeof(*root_dir_item), 1, file);
   fwrite(root_parent_dir_item, sizeof(*root_parent_dir_item), 1, file);
   // printf("Root dir inode id: %d\nRoot dir name: %s\nRoot dir parent nodeid: %d\nRoot dir parent name: %s\n", root_dir_item->inode, \
   root_dir_item->item_name, root_parent_dir_item->inode, root_parent_dir_item->item_name);
   free(root_dir);
   free(root_dir_item);
   free(root_parent_dir_item);
}

void print_i_node_bitmap(u_char *inode_bitmap){
   printf("\nInode bitmapa:\n");
        for (int i = 0 ; i < I_NODE_AMOUNT; i++){
            for (int j=7; j>=0; j--) {
                printf("%d",(inode_bitmap[i] >> j) & 0b1);
            }
        }
   printf("\n");
}

void print_data_bitmap(superblock *sb, u_char *data_bitmap){
   printf("\nData bitmapa:\n");
   for (int i = 0 ; i < sb->cluster_count; i++){
      for (int j=7; j>=0; j--) {
         printf("%d",(data_bitmap[i] >> j) & 0b1);
      }
   }
   printf("\n");
}

int find_empty_i_node_in_bitmap(u_char *inode_bitmap){
   for (int i = 0 ; i < I_NODE_AMOUNT; i++){
      for (int j=7; j>=0; j--) {
         if(((inode_bitmap[i] >> j) & 0b1) == 0)
            return ((7-j)+i*8);
      }
   }
   return -1;
}

int find_empty_data_node_in_bitmap(superblock *sb, u_char *data_bitmap){
   for (int i = 0 ; i < sb->cluster_count; i++){
      for (int j=7; j>=0; j--) {
         if(((data_bitmap[i] >> j) & 0b1) == 0)
            return ((7-j)+i*8);
      }
   }
   return -1;
}

int is_amount_of_spaces_free_in_bitmap(superblock *sb, u_char *data_bitmap, int amount){
   int x = 0;
   for (int i = 0 ; i < sb->cluster_count; i++){
      for (int j=7; j>=0; j--) {
         if(((data_bitmap[i] >> j) & 0b1) == 0){
            x++;
            // printf("X == %d\n", x);
            if(x == amount){
               return 1;
            }
         }
      }
   }
   return 0;
}

int set_i_node_bitmap_bit(u_char **inode_bitmap, int position, int mode){
   int inode_array_position = position / 8;
   int inode_bit_position = position % 8;
   if(mode == BITMAP_MODE_ADD){
      (*inode_bitmap)[inode_array_position] |= binary[inode_bit_position];
   }
   else
   {
      (*inode_bitmap)[inode_array_position] &= binary_inverse[inode_bit_position];
   }
   return position;
}

int set_data_bitmap_bit(u_char **data_bitmap, int position, int mode){
   int inode_array_position = position / 8;
   int inode_bit_position = position % 8;
   if(mode == BITMAP_MODE_ADD){
      (*data_bitmap)[inode_array_position] |= binary[inode_bit_position];
   }
   else
   {
      (*data_bitmap)[inode_array_position] &= binary_inverse[inode_bit_position];
   }
   return position;
}

void init_bitmap(u_char **bitmap, int array_element_count){
   int i = 0;
   for(; i < array_element_count; i++){
      (*bitmap)[i] = 0b00000000;
   }
}

pseudo_inode *init_pseudo_inode(int32_t nodeid, int32_t file_size, bool is_directory){
   pseudo_inode *temp = (pseudo_inode *)malloc(sizeof(pseudo_inode));
   temp->nodeid = nodeid;
   temp->file_size = file_size;
   temp->isDirectory = is_directory;
   temp->references = 1;
   temp->direct[0] = -1;
   temp->direct[1] = -1;
   temp->direct[2] = -1;
   temp->direct[3] = -1;
   temp->direct[4] = -1;
   temp->indirect1 = -1;
   temp->indirect2 = -1;
   return temp;
}


char** split_message(char *message, int *message_len){
   char *buf = malloc(strlen(message)*sizeof(char *));
   strcpy(buf, message);
   int i = 0;
   int j = 0;
   char *p = strtok (buf, " ");
   char **array = malloc(200 * sizeof(char*));

   while (p != NULL)
   {
      // printf("Message detokenized: %s\n", p);
      array[i] = malloc(strlen(p) * sizeof(char));
      array[i++] = p;
      p = strtok (NULL, " ");
   }
   for (j = 0; j < i; ++j) 
      // printf("%s\n", array[j]);

   *message_len = i;
   return array;
}


pseudo_inode *get_inode_by_id(superblock *sb, char **argv, int current_inode_pointer){
   FILE *file = fopen(argv[1], "r+b");
   pseudo_inode *inode = (pseudo_inode *)malloc(sizeof(pseudo_inode));
   fseek(file, (sb->inode_start_address)+(current_inode_pointer*sizeof(pseudo_inode)) , SEEK_SET);
   // printf("Inode position: %d\n", (sb->inode_start_address)+(current_inode_pointer*sizeof(pseudo_inode)));
   fread(inode, sizeof(pseudo_inode), 1, file);
   // printf("Inode id: %d\nFile size: %d\n", inode->nodeid, inode->file_size);
   fclose(file);
   return inode;
}



directory_item *init_directory_item(int32_t inode, char *name){
   directory_item *temp = (directory_item *)malloc(sizeof(directory_item));
   temp->inode = inode;
   strcpy(temp->item_name, name);
   return temp;
}

int is_free_ref_space(int array_size, directory_item *directories){
   int i = 0;
   for(; i < array_size; i++){
      if(directories[i].inode == -1)
         return i;  
   }
   return -1;
}

directory_item *load_directories_from_file(superblock *sb, int inode_id, u_char *inode_bitmap, u_char *data_bitmap, char **argv){
   FILE *f = fopen(argv[1], "rb");
   pseudo_inode *node = get_inode_by_id(sb, argv, inode_id);
   int ref_count = node->file_size / 16;
   int cluster_count = node->file_size / CLUSTER_SIZE;
   int i = 0;
   if(node->file_size % CLUSTER_SIZE > 0){
      cluster_count++;
   }
   // printf("Cluster count: %d\nRef_count: %d\n", cluster_count, ref_count);
   directory_item *directories = (directory_item *)calloc(ref_count, sizeof(directory_item));
   for(; i < cluster_count; i++){
         // printf("Running through the file, i = %d\n", i);
         fseek(f, sb->data_start_address + node->direct[i] * CLUSTER_SIZE, SEEK_SET);
         if(ref_count >= CLUSTER_SIZE / 16)
         {
            fread(directories + (i * 16), sizeof(*directories), CLUSTER_SIZE / 16, f);
            ref_count -= (CLUSTER_SIZE / 16);
         }
         else
         {
            fread(directories + (i * 16), sizeof(*directories), ref_count, f);
            ref_count = 0;
         }
   }
   free(node);
   // printf("Returning\n");
   fclose(f);
   return directories;
}

int get_mkdir_offset(superblock *sb, pseudo_inode *inode){
   

}

void create_new_dir_content(superblock *sb, pseudo_inode *inode, FILE *file, int32_t parent_inode){
   directory_item *current = init_directory_item(inode->nodeid, ".");
   directory_item *parent = init_directory_item(parent_inode, "..");
   fseek(file, sb->data_start_address + inode->direct[0] * CLUSTER_SIZE, SEEK_SET);
   fwrite(current, sizeof(*current), 1, file);
   fwrite(parent, sizeof(*parent), 1, file);
   free(current);
   free(parent);
}

int32_t *load_indirect_cluster_ref(superblock *sb, pseudo_inode *inode, char**argv, int *get_cluster_count){
   FILE *file = fopen(argv[1], "rb");
   if(inode->indirect1 == -1){
      return NULL;
   }
   int indirect_cluster_size_check = inode->file_size - CLUSTER_SIZE * 5;
   if(indirect_cluster_size_check < 0){
      return NULL;
   }
   int indirect_cluster_count = (inode->file_size - (CLUSTER_SIZE * 5)) / CLUSTER_SIZE;
   // printf("Indirect cluster count in method load indir cluster ref: %d\n", indirect_cluster_count);
   if((inode->file_size - CLUSTER_SIZE * 5) % CLUSTER_SIZE > 0){
      indirect_cluster_count++;
   }
   int32_t *indirect_ref = (int32_t *)calloc(indirect_cluster_count, sizeof(int32_t));
   fseek(file, sb->data_start_address + inode->indirect1 * CLUSTER_SIZE, SEEK_SET);
   fread(indirect_ref, sizeof(*indirect_ref), indirect_cluster_count, file);
   *get_cluster_count = indirect_cluster_count;
   // printf("Reading from data bit [%d]: %d\n", inode->indirect1, indirect_ref[0]);
   fclose(file);
   return indirect_ref;
}

directory_item *load_directory_chunk(superblock *sb, int32_t data_bitmap_position, int32_t *array_size, char **argv, pseudo_inode *inode){
   FILE *file = fopen(argv[1], "rb");
   int32_t dir_count = get_directory_count(sb, argv, inode, data_bitmap_position);
   // printf("Directory count in load dir chunk: %d\n", dir_count);
   directory_item *directories = (directory_item *)calloc(dir_count, sizeof(directory_item));
   fseek(file, sb->data_start_address + data_bitmap_position * CLUSTER_SIZE, SEEK_SET);
   fread(directories, sizeof(*directories), dir_count, file);
   *array_size = dir_count;
   fclose(file);
   return directories;
}

int32_t get_directory_count(superblock *sb, char **argv, pseudo_inode *inode, int32_t data_bit){
   FILE *file = fopen(argv[1], "rb");
   int cluster_count = 0;
   int testing_var = 0;
   int i = 0;
   int32_t *all_refs = get_all_refs(sb, inode, argv, &cluster_count);
   // printf("Indirect cluster count in directory count: %d\n", cluster_count);
   // puts("Printing all refs in get dir count!");
   // print_all_refs(all_refs, cluster_count);
   for(i = 0; i < cluster_count; i++){
      if(all_refs[i] == data_bit){
         if((inode->file_size - (i + 1)*CLUSTER_SIZE) > 0){
            return CLUSTER_SIZE / 16;
         }
         else{
            return (inode->file_size - i*CLUSTER_SIZE) / 16;
         }
      }
   }
}

int32_t *get_all_refs(superblock *sb, pseudo_inode *inode, char**argv, int *get_cluster_count){
   int32_t *indirect_refs = load_indirect_cluster_ref(sb, inode, argv, get_cluster_count);
   int32_t *all_refs = (int32_t *)calloc(5 + *get_cluster_count, sizeof(int32_t));
   int i = 0;
   for(; i < 5; i++){
      all_refs[i] = inode->direct[i];
      // printf("Ref %d: %d\n", i, all_refs[i]);
   }
   for(i = 0; i < *get_cluster_count; i++){
      all_refs[i+5] = indirect_refs[i];
      // printf("Indirect ref %d: %d\n", i+5, indirect_refs[i]);
   }
   *get_cluster_count += 5;
   return all_refs; 
}

void print_inode_info(pseudo_inode *inode){
   printf("Inode number: %d\nIs directory: %s\nReferences: %d\nFile size: %d\nDirect [0]: %d\nDirect [1]: %d\nDirect [2]: %d\nDirect [3]: %d\nDirect [4]: %d\nIndirect: %d\n", \
   inode->nodeid, (inode->isDirectory ? "True" : "False"), inode->references, inode->file_size, \
   inode->direct[0], inode->direct[1], inode->direct[2], \
   inode->direct[3], inode->direct[4], inode->indirect1);
}

void print_all_refs(int32_t *refs, int count, bool show_all){
   int i = 0;
   for(; i < count; i++){
      if(show_all){
         printf("Ref [%d]: %d\n", i, refs[i]);
      }
      else
      {
         if(refs[i] != -1){
            printf("Ref [%d]: %d\n", i, refs[i]);
         }
      }
   }
}

void change_path(char **path, char *cd_result){
   // printf("Path: %s\n", *path);
   int flag = 0;
   int i = 0;
   char *tmp = (char *)calloc(1000, sizeof(char));
   if(!strcmp(*path, "/") && (!strcmp(cd_result, "..") || !strcmp(cd_result, "."))){
      return;
   }
   if(!strcmp(cd_result, "..")){
      // puts("Removing last path part!");
      // printf("Path length: %d\n", strlen(*path));
      for(i = (strlen(*path) - 1); i >= 0; i--){
         // printf("%d %c\n", i, (*path)[i]);
         if((*path)[i] == '/'){
            if(flag){
               // printf("I equals: %d\n", i);
               strncpy(tmp, *path, i + 1);
               // printf("%s\n", tmp);
               strcpy(*path, tmp);
               // printf("%s\n", *path);
               return;
            }
            else{
               flag = 1;
            }
         }
      }
   }
   if(!strcmp(cd_result, ".")){
      return;
   }
   strcat(*path, cd_result);
   strcat(*path, "/");
   
}

void write_new_directory_item_to_memory(superblock *sb, pseudo_inode *current_dir, char **argv, u_char **data_bitmap, FILE *file, char *args, int32_t inode_id){
   int new_dir_item_written_flag = 0;
   int cluster_count = 0;
   int i = 0;
   directory_item *directories = NULL;
   int array_size = 0;
   int free_space = 0;
   /* GET ALL REFS FROM CURRENT INODE */
   int32_t *all_refs = get_all_refs(sb, current_dir, argv, &cluster_count);
   directory_item *new_dir_item = init_directory_item(inode_id, args);
   print_all_refs(all_refs, cluster_count, true);
   /* CHECK FOR FREE SPACE IN INODE REFS */
      for(; i < cluster_count; i++){
         if(all_refs[i] != -1){
            directories = load_directory_chunk(sb, all_refs[i], &array_size, argv, current_dir);
            free_space = is_free_ref_space(array_size, directories);
            /* IF FREE SPACE, WRITE IN THERE*/
            if(free_space != -1){
               // puts("Im in free space area!");
               fseek(file, sb->data_start_address + all_refs[i] * CLUSTER_SIZE + free_space * sizeof(directory_item), SEEK_SET);
               // printf("Position written: %d\n", sb->data_start_address + all_refs[i] * CLUSTER_SIZE + free_space * sizeof(directory_item));
               fwrite(new_dir_item, sizeof(*new_dir_item), 1, file);
               new_dir_item_written_flag = 1;
            }         
         }
         else break;
      }
      /* CHECK IF DIRECTORY HAS BEEN REWRITTEN TO AN OLD DIRECTORY ITEM */
      if(!new_dir_item_written_flag){
         // printf("Im in flagged area!\nNumber i equals: %d\n", i);
         /* CHECK FOR MODULO CLUSTER SIZE */
         if(current_dir->file_size % CLUSTER_SIZE == 0){
            if(current_dir->file_size < CLUSTER_SIZE * 5){
               current_dir->direct[i] = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
               // printf("Created new data bit at: %d\n", current_dir->direct[i]);
               fseek(file, sb->data_start_address + current_dir->direct[i] * CLUSTER_SIZE, SEEK_SET);
               fwrite(new_dir_item, sizeof(*new_dir_item), 1, file);
            }
            else{
               if(current_dir->file_size % (CLUSTER_SIZE * 5) == 0 && current_dir->indirect1 == -1){
                  current_dir->indirect1 = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
                  // printf("Idirect ref created: %d\n", current_dir->indirect1);
               }
               int32_t *indirect = load_indirect_cluster_ref(sb, current_dir, argv, &cluster_count);
               // printf("Cluster count while making indirect: %d\n", cluster_count);
               indirect = (int32_t *)realloc(indirect, sizeof(int32_t) * (cluster_count + 1));
               indirect[cluster_count] = set_data_bitmap_bit(data_bitmap, find_empty_data_node_in_bitmap(sb, *data_bitmap), BITMAP_MODE_ADD);
               // printf("New indirect cluster created! Data bit: %d\n", indirect[cluster_count]);
               fseek(file, sb->data_start_address + current_dir->indirect1 * CLUSTER_SIZE, SEEK_SET);
               fwrite(indirect, sizeof(*indirect), cluster_count + 1, file);
               fseek(file, sb->data_start_address + indirect[cluster_count] * CLUSTER_SIZE, SEEK_SET);
               fwrite(new_dir_item, sizeof(*new_dir_item), 1, file);
            }
         }
         else{
            // printf("Im here!\n");
            i--;
            if(current_dir->file_size < CLUSTER_SIZE * 5){
               // printf("Writing to second place of an existind data_bit: %d\n", current_dir->direct[i]);
               // printf("I equals: %d\n", i);
               fseek(file, sb->data_start_address + current_dir->direct[i] * CLUSTER_SIZE + array_size * 16, SEEK_SET);
               fwrite(new_dir_item, sizeof(*new_dir_item), 1, file);
            }
            else{
               // printf("Writing to second place of idirect reference: %d\n", all_refs[i]);
               fseek(file, sb->data_start_address + all_refs[i] * CLUSTER_SIZE + array_size * 16, SEEK_SET);
               fwrite(new_dir_item, sizeof(*new_dir_item), 1, file);
            }
         }
         current_dir->file_size += 16;
      }
      /* REWRITE INODE */
      fseek(file, sb->inode_start_address + current_dir->nodeid * sizeof(*current_dir), SEEK_SET);
      fwrite(current_dir, sizeof(*current_dir), 1, file);
}

int get_inode_id_by_name(superblock *sb, char **argv, char *args, pseudo_inode *current_dir){
   FILE *file = fopen(argv[1], "rb");
   int cluster_count = 0;
   int array_size = 0;
   int i = 0, j = 0;
   int32_t *all_refs = get_all_refs(sb, current_dir, argv, &cluster_count);
   for(i = 0; i < cluster_count; i++){
      directory_item *directories = load_directory_chunk(sb, all_refs[i], &array_size, argv, current_dir);
      for(j = 0; j < array_size; j++){
         if(!strcmp(directories[j].item_name, args)){
            return directories[j].inode;
         }
      }
   }
   return -1;
}

void remove_directory_item_record_from_inode_data(superblock *sb, char **argv, pseudo_inode *inode, char *args){
   FILE *file = fopen(argv[1], "r+b");
   int cluster_count = 0;
   int array_size = 0;
   int i = 0, j = 0;
   int32_t *all_refs = get_all_refs(sb, inode, argv, &cluster_count);
   print_all_refs(all_refs, cluster_count, true);
   directory_item *directories = NULL;
   for(i = 0; i < cluster_count; i++){
      if(all_refs[i] != -1){
         directories = load_directory_chunk(sb, all_refs[i], &array_size, argv, inode);
         for(j = 0; j < array_size; j++){
            if(directories[j].inode != -1){
               if(!strcmp(directories[j].item_name, args)){
                  directories[j].inode = -1;
                  fseek(file, sb->data_start_address + all_refs[i] * CLUSTER_SIZE, SEEK_SET);
                  fwrite(directories, sizeof(*directories), array_size, file);
                  fclose(file);
                  return;
               }
            }
         }
      }
   }
   fclose(file);
}

int check_is_name_exists(superblock *sb, char **argv, pseudo_inode *inode, char *args){
   FILE *file = fopen(argv[1], "rb");
   int cluster_count = 0;
   int array_size = 0;
   int i = 0, j = 0;
   int32_t *all_refs = get_all_refs(sb, inode, argv, &cluster_count);
   for (i = 0; i < cluster_count; i++)
   {
      directory_item *directories = load_directory_chunk(sb, all_refs[i], &array_size, argv, inode);
      for(j = 0; j < array_size; j++){
         if(!strcmp(directories[j].item_name, args) && directories[j].inode != -1){
            return 1;
         }
      }
   }
   return 0;
   
}


char **detokenize_path(char *path, int *token_count, bool is_absolute_path){
   char *buf = malloc(strlen(path)*sizeof(char *));
   strcpy(buf, path);
   int i = 0;
   int j = 0;
   char *p = strtok (buf, "/");
   char **array = malloc(200 * sizeof(char*));

   while (p != NULL)
   {
      // printf("Message detokenized: %s\n", p);
      array[i] = malloc(strlen(p) * sizeof(char));
      array[i++] = p;
      p = strtok (NULL, "/");
   }
   // for (j = 0; j < i; ++j) 
   //    printf("%s\n", array[j]);

   *token_count = i;
   return array;
}

int check_path_viable(superblock *sb, char **argv, char **detokenized_path, int token_count, int current_inode_pointer){
   FILE *file = fopen(argv[1], "rb");
   pseudo_inode *current = get_inode_by_id(sb, argv, current_inode_pointer);
   int i = 0;
   int32_t temporary_inode_pointer = current_inode_pointer;
   for(i = 0; i < token_count; i++){
      int next_inode = get_inode_id_by_name(sb, argv, detokenized_path[i], current);
      if(next_inode != -1){
         current = get_inode_by_id(sb, argv, next_inode);
         if(!current->isDirectory){
            // puts("Is not directory!");
            return 0;
         }
      }
      else{
         // puts("Path not viable!");
         return 0;
      }

   }
   return 1;
}


void change_directory(superblock *sb, int *current_i_node_pointer, char *args, char **argv, char **path){
    FILE *file = fopen(argv[1], "rb");
    int i = 0, j = 0;
    int array_size = 0;
    pseudo_inode *inode = get_inode_by_id(sb, argv, *current_i_node_pointer);
    if(!inode->isDirectory){
        puts("NOT A DIRECTORY!");
        free(inode);
        return;
    }
    int cluster_count = inode->file_size / CLUSTER_SIZE;
    if(inode->file_size % CLUSTER_SIZE > 0){
        cluster_count++;
    }

    for(i = 0; i < cluster_count; i++){
        directory_item *directories = load_directory_chunk(sb, inode->direct[i], &array_size, argv, inode);
        for(j = 0; j < array_size; j++){
            // printf("Inode: %d %s\n", directories[j].inode, directories[j].item_name);
            if(!strcmp(args, directories[j].item_name) && directories[j].inode != -1)
            {
               //  printf("Found it! Inode is: %d\n", directories[j].inode);
                *current_i_node_pointer = directories[j].inode;
                change_path(path, directories[j].item_name);
                return;
            }
        }
    }
    puts("Invalid directory name!");
}

char *remove_last_path_part(char *path){
   int i = strlen(path);
   char *temp = (char *)calloc(1000, sizeof(char));
   for(; i >=0 ; i--){
      if(path[i] == '/'){
         strncpy(temp, path, i);
         break;
      }
   }
   strcpy(path, temp);
   return temp;
}

char **test_path_detokenization(superblock *sb, char **argv, int current_inode_pointer, char *args, int *path_count){
    int count = 0;
    char **detokenized_path = detokenize_path(args, &count, false);
    if(check_path_viable(sb, argv, detokenized_path, count, current_inode_pointer)){
      //   printf("Path exists!\n");
        *path_count = count;
        return detokenized_path;
    }
    else{
      //   printf("Path not viable!\n");
        *path_count = -1;
        return NULL;
    }
   //  printf("Token count: %d\n", count);
}
