CC = gcc
BIN = fs
OBJ = main.o commands.o utils.o


$(BIN): $(OBJ)
	$(CC) *.o -o ${BIN}

%.o: %.c
	$(CC) -c *.c